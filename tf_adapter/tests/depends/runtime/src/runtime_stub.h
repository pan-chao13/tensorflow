/**
*
* Copyright (C) Huawei Technologies Co., Ltd. 2023. All Rights Reserved.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#ifndef DEPENDS_RUNTIME_STUB_H
#define DEPENDS_RUNTIME_STUB_H

void setMockStub(bool g_stub);

#endif //DEPENDS_ASCENDCL_STUB_H